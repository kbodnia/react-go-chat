import React, { Component } from 'react';
import './App.css';
import Header from './components/Header';
import { connect, sendMsg, sendFile } from './api';
import ChatHistory from './components/ChatHistory';
import ChatInput from './components/ChatInput';
import MyDropZone from './components/MyDropZone'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chatHistory: []
    }
  }

  send(event) {
    if (event.keyCode === 13) {
      let msg = event.target.value.trim();
      if (msg !== '') {
        sendMsg(msg);
        event.target.value = '';
      }
    }
  }

  componentDidMount() {
    connect((msg) => {
      console.log('New Message received');

      this.setState(prevState => ({
        chatHistory: [...this.state.chatHistory, msg]
      }));

      console.log(this.state);
    });
  }

  render() {
    return (
      <div className="App">

        <Header />
        <ChatHistory chatHistory={this.state.chatHistory} />
        <MyDropZone sendFile={sendFile}/>
        <ChatInput send={this.send} />

      </div>
    );
  }
}

export default App;
