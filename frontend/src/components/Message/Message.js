import React, { Component } from 'react';
import './Message.scss';

class Message extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: JSON.parse(this.props.message),
    };
  }

  render() {
    return (
      <div className="Message">{(() => {
        switch(this.state.message.type) {
          case 2:
            return (<img src={this.state.message.body} />);
          default:
            return this.state.message.body;
        }
      })()}</div>
    );
  }
}

export default Message;
