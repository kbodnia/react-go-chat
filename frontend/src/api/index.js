// api/index.js
var socket = new WebSocket('ws://localhost:8080/ws');

let connect = cb => {
  console.log('connecting');

  socket.onopen = () => {
    console.log('Successfully Connected');
  };

  socket.onmessage = msg => {
    console.log(msg);
    cb(JSON.parse(JSON.stringify(msg.data)));
  };

  socket.onclose = event => {
    console.log('Socket Closed Connection: ', event);
  };

  socket.onerror = error => {
    console.log('Socket Error: ', error);
  };
};

const sendMsg = msg => {
  console.log('sending msg: ', msg);
  socket.send(msg);
};

const sendFile = file => {
  console.log('sending file: ', file);
  const reader = new FileReader();
  var rawData = new ArrayBuffer();
  reader.onload = function(e) {
    rawData = e.target.result;
    socket.send(rawData);
  }
  reader.readAsArrayBuffer(file);
};

export { connect, sendMsg, sendFile };
