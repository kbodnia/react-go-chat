module websocket-chat

go 1.15

require (
	github.com/google/uuid v1.1.2
	github.com/gorilla/websocket v1.4.2
	github.com/minio/minio-go/v7 v7.0.5
)
