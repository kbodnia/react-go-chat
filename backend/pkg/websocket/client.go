package websocket

import (
	"fmt"
	"log"
	"websocket-chat/pkg/uploader"

	"github.com/gorilla/websocket"
)

type Client struct {
	ID       string
	Conn     *websocket.Conn
	Pool     *Pool
	Uploader *uploader.Uploader
}

type Message struct {
	Type int    `json:"type"`
	Body string `json:"body"`
}

func (c *Client) Read() {
	defer func() {
		c.Pool.Unregister <- c
		_ = c.Conn.Close()
	}()

	for {
		messageType, p, err := c.Conn.ReadMessage()
		if err != nil {
			log.Println(err)
			return
		}
		var message Message

		switch messageType {
		case 2:
			url := c.Uploader.Upload(p)
			message = Message{Type: 2, Body: url}
			break
		default:
			message = Message{Type: messageType, Body: string(p)}
			break
		}

		c.Pool.Broadcast <- message
		fmt.Printf("Message Received: %+v\n", message)
	}
}
