package uploader

import (
	"bytes"
	"context"
	"log"
	"net/url"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

type Uploader struct {
	minioClient *minio.Client
	ctx         context.Context
	bucketName  string
}

var (
	i    Uploader
	once sync.Once
)

func NewUploader() *Uploader {

	once.Do(func() {

		ctx := context.Background()
		endpoint := "127.0.0.1:9000"
		accessKeyID := "AKIAIOSFODNN7EXAMPLE"
		secretAccessKey := "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"

		// Initialize minio client object.
		minioClient, err := minio.New(endpoint, &minio.Options{
			Creds:  credentials.NewStaticV4(accessKeyID, secretAccessKey, ""),
			Secure: false,
		})
		if err != nil {
			log.Fatalln(err)
		}

		bucketName := "awesome-chat"
		location := "eu-west-4"

		err = minioClient.MakeBucket(ctx, bucketName, minio.MakeBucketOptions{Region: location})
		if err != nil {
			// Check to see if we already own this bucket (which happens if you run this twice)
			exists, errBucketExists := minioClient.BucketExists(ctx, bucketName)
			if errBucketExists == nil && exists {
				log.Printf("We already own %s\n", bucketName)
			} else {
				log.Fatalln(err)
			}
		} else {
			log.Printf("Successfully created %s\n", bucketName)
		}

		i = Uploader{
			minioClient,
			ctx,
			bucketName,
		}

	})

	return &i
}

func (u *Uploader) Upload(f []byte) string {

	objectName := uuid.New().String() + ".jpg"
	contentType := "image/jpg"

	_, err := u.minioClient.PutObject(
		u.ctx,
		u.bucketName,
		objectName,
		bytes.NewReader(f),
		int64(len(f)),
		minio.PutObjectOptions{ContentType: contentType},
	)
	if err != nil {
		log.Fatalln(err)
	}

	reqParams := make(url.Values)
	reqParams.Set("response-content-disposition", "attachment; filename=\""+objectName+"\"")
	objectUrl, _ := u.minioClient.PresignedGetObject(
		u.ctx,
		u.bucketName,
		objectName,
		time.Hour,
		reqParams,
	)

	log.Printf("Successfully generated public url\n", objectUrl.String())

	return objectUrl.String()
}
