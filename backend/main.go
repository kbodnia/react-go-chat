package main

import (
	"fmt"
	"net/http"
	"websocket-chat/pkg/uploader"

	"websocket-chat/pkg/websocket"
)

func serveWs(
	pool *websocket.Pool,
	uploader *uploader.Uploader,
	w http.ResponseWriter,
	r *http.Request,
) {
	conn, err := websocket.Upgrade(w, r)
	if err != nil {
		_, _ = fmt.Fprintf(w, "%+V\n", err)
	}

	client := websocket.Client{
		Conn: conn,
		Pool: pool,
		Uploader: uploader,
	}

	pool.Register <- &client
	client.Read()
}

func setupRoutes() {
	pool := websocket.NewPool()
	go pool.Start()

	upl := uploader.NewUploader()

	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		serveWs(pool, upl, w, r)
	})
}

func main() {
	fmt.Println("Distributed Chat App v0.01")
	setupRoutes()

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Println("could not start the server")
	}
}
